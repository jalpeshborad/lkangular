'use strict';

// Define the `phonecatApp` module
angular.module('phonecatApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'ngController',
  'phoneDetail',
  'phoneList',
  'reportList',
]);
