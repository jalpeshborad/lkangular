'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('phonecatApp').
  component('reportList', {
    templateUrl: 'phone-list/phone-list.template.html',
    controller: ['ReportListController',
      function ReportListController(report) {
        this.reports = report.query();
      }
    ]
  });
